/*
Copyright (c) 2017 Ross McWhannell
Distributed under the MIT License.
(See accompanying LICENSE file or https://opensource.org/licenses/MIT)
*/

#include <FnGeolib/op/FnGeolibOp.h>
#include <FnGeolibServices/FnXFormUtil.h>
#include <FnGeolibServices/FnGeolibCookInterfaceUtilsService.h>

#include <OpenEXR/ImathVec.h>
#include <OpenEXR/ImathMatrix.h>
#include <OpenEXR/ImathBox.h>

#include <algorithm>

namespace
{
// Create a collection from locations that are are inside a given bounding box.

class CollectionCreateBoundsOp : public Foundry::Katana::GeolibOp
{
public:

    // Boilerplate that indicates the Op's cook() function is safe to be called
    // concurrently.

    static void setup(Foundry::Katana::GeolibSetupInterface& interface)
    {
        interface.setThreading(
            Foundry::Katana::GeolibSetupInterface::ThreadModeConcurrent);
    }

    // Get the world space transform for a given location and return the matrix.

    static Imath::M44d getWorldSpaceXForm(Foundry::Katana::GeolibCookInterface& interface, const std::string& location, const float& time)
    {
        FnAttribute::GroupAttribute xformGroupAttr = Foundry::Katana::GetGlobalXFormGroup(interface, location);
        
        std::pair<FnAttribute::DoubleAttribute, bool> xformMatrixAttr = Foundry::Katana::FnXFormUtil::CalcTransformMatrixAtTime(xformGroupAttr, time);
        FnAttribute::DoubleConstVector xformMatrix = xformMatrixAttr.first.getNearestSample(time);
        
        return Imath::M44d(reinterpret_cast<const double(*)[4]>(xformMatrix.data()));
    }

    // Cook the location.

    static void cook(Foundry::Katana::GeolibCookInterface& interface)
    {
        // Validate input args.

        FnAttribute::StringAttribute locationAttr = interface.getOpArg("location");
        FnAttribute::StringAttribute nameAttr = interface.getOpArg("name");
        FnAttribute::StringAttribute celAttr = interface.getOpArg("CEL");
        FnAttribute::StringAttribute boundsSourceAttr = interface.getOpArg("boundsSource");
        FnAttribute::StringAttribute modeAttr = interface.getOpArg("mode");
        FnAttribute::StringAttribute partialIntersectionsAttr = interface.getOpArg("partialIntersections");

        if (!locationAttr.isValid() || 
            !nameAttr.isValid() || 
            !celAttr.isValid() ||
            !boundsSourceAttr.isValid() ||
            !modeAttr.isValid() ||
            !partialIntersectionsAttr.isValid())
        {
            interface.stopChildTraversal();
            return;
        }

        FnAttribute::DoubleAttribute boundsSourceBoundsAttr = interface.getOpArg("_boundsSourceBounds");
        FnAttribute::DoubleAttribute boundsSourceMatrixAttr = interface.getOpArg("_boundsSourceMatrix");

        // If this is the first location cooked calculate source bounds data and pass along to child traversals.

        if (!boundsSourceBoundsAttr.isValid())
        {
            std::string boundsSource = boundsSourceAttr.getValue();
            if (!interface.doesLocationExist(boundsSource))
            {
                interface.stopChildTraversal();
                return;
            }

            interface.prefetch(boundsSource);
            boundsSourceBoundsAttr = interface.getAttr("bound", boundsSource);
            if (!boundsSourceBoundsAttr.isValid())
            {
                interface.stopChildTraversal();
                return;                
            }            

            // Find the inverse world space transform for the boundsSource so we can transform test points into
            // the boundsSource object space for intersection testing.

            Imath::M44d xformMatrix = getWorldSpaceXForm(interface, boundsSource, Foundry::Katana::GetCurrentTime(interface));
            Imath::M44d invXFormMatrix = xformMatrix.inverse();
            boundsSourceMatrixAttr = FnAttribute::DoubleAttribute(reinterpret_cast<const double*>(invXFormMatrix.getValue()), 16, 1);

            FnAttribute::GroupBuilder gb;

            gb.update(interface.getOpArg());
            gb.set("_boundsSourceBounds", boundsSourceBoundsAttr);
            gb.set("_boundsSourceMatrix", boundsSourceMatrixAttr);

            interface.replaceChildTraversalOp(interface.getOpType(), gb.build());
        }

        // If a collection location is specified create the collection, otherwise match the CEL
        // and test if the location intersects with the source bounds. 

        if (interface.getInputLocationPath() == locationAttr.getValue())
        {
            std::string boundsIdentifier = boundsSourceAttr.getValue();;
            std::replace(boundsIdentifier.begin(), boundsIdentifier.end(), '/', '_');

            FnAttribute::StringAttribute collectionCEL("(//*){hasattr(\"intersectsBounds." + boundsIdentifier + "\")}");
            interface.setAttr("collections." + nameAttr.getValue() + ".cel", collectionCEL);
        }
        else
        {
            // Check if the current location should be tested for intersections.

            FnGeolibServices::FnGeolibCookInterfaceUtils::MatchesCELInfo info;
            FnGeolibServices::FnGeolibCookInterfaceUtils::matchesCEL(info, interface, celAttr);

            if (!info.canMatchChildren)
            {
                interface.stopChildTraversal();
            }
            
            if (!info.matches)
            {
                return;   
            }

            // If we are in points mode check to see if we have points, if we are in bounds mode check to 
            // see we have bounds.

            bool pointsMode = modeAttr.getValue() == "Points";

            if ((pointsMode && !interface.getAttr("geometry.point.P").isValid()) ||
                (!pointsMode && !interface.getAttr("bound").isValid()))
            {
                return;
            }

            // Get the points of either the geometry or the bounds, transform them into the object space
            // of the sourceBounds and test to see if they fall inside the sourceBounds. 

            std::string location = interface.getInputLocationPath();
            float time = Foundry::Katana::GetCurrentTime(interface);

            bool includePartialIntersections = partialIntersectionsAttr.getValue() == "Include";
            bool isIntersected = !includePartialIntersections; 

            FnAttribute::DoubleConstVector boundsSourceBounds = boundsSourceBoundsAttr.getNearestSample(time);
            FnAttribute::DoubleConstVector boundsSourceMatrix = boundsSourceMatrixAttr.getNearestSample(time);
            
            Imath::Box3d boundsSourceBox(
                Imath::V3d(boundsSourceBounds[0], boundsSourceBounds[4], boundsSourceBounds[2]), 
                Imath::V3d(boundsSourceBounds[3], boundsSourceBounds[1], boundsSourceBounds[5])
            );

            Imath::M44d boundsSourceXFormMatrix = reinterpret_cast<const double(*)[4]>(boundsSourceMatrix.data());
            Imath::M44d locationXFormMatrix = getWorldSpaceXForm(interface, location, time);

            if (pointsMode)
            {
                FnAttribute::FloatAttribute pointsAttr = interface.getAttr("geometry.point.P");
                FnAttribute::FloatConstVector points = pointsAttr.getNearestSample(time);

                Imath::M44d xformMatrix = locationXFormMatrix * boundsSourceXFormMatrix;

                for (uint64_t i = 0; i < points.size() / 3; ++i)
                {
                    Imath::V3d srcPoint((double)points[i*3], (double)points[i*3+1], (double)points[i*3+2]);
                    Imath::V3d dstPoint;

                    xformMatrix.multVecMatrix(srcPoint, dstPoint);

                    if (boundsSourceBox.intersects(dstPoint))
                    {
                        if (includePartialIntersections)
                        {
                            isIntersected = true;
                            break;
                        }
                    }
                    else
                    {
                        if (!includePartialIntersections)
                        {
                            isIntersected = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                FnAttribute::DoubleAttribute locationBoundsAttr = interface.getAttr("bound");

                std::vector<Foundry::Katana::BoundPoint> points;
                Foundry::Katana::GetTransformedBoundAttrValue(points, locationBoundsAttr, time, locationXFormMatrix.getValue());

                for (int i = 0; i < 8; ++i)
                {
                    Imath::V3d srcPoint(points[i].x, points[i].y, points[i].z);
                    Imath::V3d dstPoint;

                    boundsSourceXFormMatrix.multVecMatrix(srcPoint, dstPoint);

                    if (boundsSourceBox.intersects(dstPoint))
                    {
                        if (includePartialIntersections)
                        {
                            isIntersected = true;
                            break;
                        }
                    }
                    else
                    {
                        if (!includePartialIntersections)
                        {
                            isIntersected = false;
                            break;
                        }
                    }
                }
            }

            // If the location intersects, create an intersectsBounds attribute and stop child traversal.
            // Stopping child traversal assumes that in bounds mode the bounds in the scene are sane, 
            // ie. a parent's bounds will tightly bound all it's children's bounds.

            if (isIntersected)
            {   
                std::string boundsIdentifier = boundsSourceAttr.getValue();
                std::replace(boundsIdentifier.begin(), boundsIdentifier.end(), '/', '_');

                interface.setAttr("intersectsBounds." + boundsIdentifier, FnAttribute::IntAttribute(1), true);

                interface.stopChildTraversal();
            }
        }
    }
};
DEFINE_GEOLIBOP_PLUGIN(CollectionCreateBoundsOp)
}  // namespace

void registerPlugins()
{
    REGISTER_PLUGIN(CollectionCreateBoundsOp, "CollectionCreateBounds", 0, 1);
}
