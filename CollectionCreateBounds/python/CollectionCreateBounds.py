# Copyright (c) 2017 Ross McWhannell
# Distributed under the MIT License.
# (See accompanying LICENSE file or https://opensource.org/licenses/MIT)

def registerCollectionCreateBounds():
    from Katana import Nodes3DAPI
    from Katana import FnAttribute

    def buildCollectionCreateBoundsOpChain(node, interface):
        """
        Defines the callback function used to create the Ops chain for the
        node type being registered.

        @type node: C{Nodes3DAPI.NodeTypeBuilder.CubeMaker}
        @type interface: C{Nodes3DAPI.NodeTypeBuilder.BuildChainInterface}
        @param node: The node for which to define the Ops chain
        @param interface: The interface providing the functions needed to set
            up the Ops chain for the given node.
        """
        # Get the current frame time
        frameTime = interface.getGraphState().getTime()

        # Set the minimum number of input ports
        interface.setMinRequiredInputs(1)

        argsGb = FnAttribute.GroupBuilder()

        # Parse node parameters
        locationParam = node.getParameter('location')
        if locationParam:
            argsGb.set('location', locationParam.getValue(frameTime))

        nameParam = node.getParameter('name')
        if nameParam:
            argsGb.set('name', nameParam.getValue(frameTime))

        celParam = node.getParameter('CEL')
        if celParam:
            argsGb.set('CEL', celParam.getValue(frameTime))

        boundsSourceParam = node.getParameter('boundsSource')
        if boundsSourceParam:
            argsGb.set('boundsSource', boundsSourceParam.getValue(frameTime))

        modeParam = node.getParameter('mode')
        if modeParam:
            argsGb.set('mode', modeParam.getValue(frameTime))

        partialIntersectionsParam = node.getParameter('partialIntersections')
        if partialIntersectionsParam:
            argsGb.set('partialIntersections', partialIntersectionsParam.getValue(frameTime))

        # Add the CubeMaker Op to the Ops chain
        interface.appendOp('CollectionCreateBounds', argsGb.build())


    nodeTypeBuilder = Nodes3DAPI.NodeTypeBuilder('CollectionCreateBounds')

    # Add an input port
    nodeTypeBuilder.setInputPortNames(('in',))

    # Build the node's parameters
    gb = FnAttribute.GroupBuilder()
    gb.set('location', FnAttribute.StringAttribute('/root/world/geo'))
    gb.set('name', FnAttribute.StringAttribute(''))
    gb.set('CEL', FnAttribute.StringAttribute(''))
    gb.set('boundsSource', FnAttribute.StringAttribute(''))
    gb.set('mode', FnAttribute.StringAttribute('Bounds'))
    gb.set('partialIntersections', FnAttribute.StringAttribute('Exclude'))

    # Set the parameters template
    nodeTypeBuilder.setParametersTemplateAttr(gb.build())

    # Set parameter hints
    nodeTypeBuilder.setHintsForParameter('location', {
        'widget':  'scenegraphLocation', 
        'help':    '<p>The scenegraph location at which to create the collection.</p>'
    })

    nodeTypeBuilder.setHintsForParameter('name', {
        'help':    '<p>The name of the collection to be created.</p>'
    })
    
    nodeTypeBuilder.setHintsForParameter('CEL', {
        'widget':  'cel',
        'help':    '<p>This CEL will determine which locations to test against the bounds source for intersections.</p>'
    })

    nodeTypeBuilder.setHintsForParameter('boundsSource', {
        'widget':  'scenegraphLocation',
        'help':    '<p>The location to use as the bounds source other locations will be tested against.</p>'
    })

    nodeTypeBuilder.setHintsForParameter('mode', {
        'widget':  'popup', 
        'options': 'Bounds|Points',
        'help':    '<p>Whether to test the locations for intersections simply by their bounds or whether to look at ' \
                   'all geometric points.</p><p>Points mode can be useful in edge cases where you have irregularly shaped ' \
                   'meshes whose bounds do not reflect the volume of the mesh in a meaningful way.</p><p><b>Important:</b> ' \
                   'Points mode will only work on geometry (locations with geometry.point.P) and can be considerably slower ' \
                   'especially with large meshes.</p>' 
    })

    nodeTypeBuilder.setHintsForParameter('partialIntersections', {
        'widget':  'popup', 
        'options': 'Include|Exclude',
        'help':    '<p>Whether to include intersections that don''t completely sit inside the bounds source.</p>' \
                   '<p>If at least one but not all points tested sit inside the bounds source this is considered ' \
                   'a partial intersection.</p>' 
    })

    # Set the callback responsible to build the Ops chain
    nodeTypeBuilder.setBuildOpChainFnc(buildCollectionCreateBoundsOpChain)

    # Build the new node type
    nodeTypeBuilder.build()

registerCollectionCreateBounds()